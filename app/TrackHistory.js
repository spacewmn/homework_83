const router = require("express").Router();
const TrackHistory = require("../models/TrackHistory");
const User = require('../models/User');

    router.post("/", async (req, res) => {
        const token = req.get("Authorization");
        if (!token) {
            return res.status(401).send({error: 'No token presented'});
        }
        const user = await User.findOne({token});
        if (!user) {
            return res.status(401).send({error: 'Wrong token'});
        }
        const userId = user._id;
        const userInfo = {...req.body, user: userId}
        let trackHistory = new TrackHistory(userInfo);
        try {
            await trackHistory.save();
            res.send(trackHistory);
        } catch(e) {
            console.log(e);
            res.status(400).send(e);
        }
    });

module.exports = router;