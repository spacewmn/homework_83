const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const app = express();
const port = 8000;
const artists = require("./app/Artist");
const albums = require("./app/Album");
const tracks = require("./app/Track")
const users = require("./app/Users");
const trackHistory = require("./app/TrackHistory");

app.use(cors());
app.use(express.json());
app.use(express.static("public"));

const run = async () => {
    await mongoose.connect("mongodb://localhost/lesson82", {useNewUrlParser: true});

    app.use("/artists", artists());
    app.use("/albums", albums());
    app.use("/tracks", tracks());
    app.use("/users", users);
    app.use("/track_history", trackHistory)

    console.log("Connected to mongo DB");

    app.listen(port, () => {
        console.log(`Server started at http://localhost:${port}`);
    });
};

run().catch(console.log);